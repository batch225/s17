// Basic Functions

// [Section] Function Declaration

function printName() {
	console.log("My name is John")
};

printName();


/*
	The statements and instructions inside a function is not immediately executed when the function is defined.
	They are run/executed when a function is invoked.
	To invoke a declared function, add the name of the function and a parenthesis.

*/

printName();

// Hoisting - javascript behavior for certain variables and functions to run to use them before their declaration

// Function Expression

let variableFunction = function myGreetings() {
	console.log("Hello");
}

variableFunction();

// Reassigning Functions

variableFunction = function myGreetings() {
	console.log("Updated Hello");
}

variableFunction();

// Function Scoping

// Scope is the accessibility(visibility) of variables

/*

	a. local/block scope
	b. global scope
	c. function scope

*/

// Local Variable

{
	let localVar = "Armando Perez";
	console.log(localVar);
}

let globalVar = "Mr. Worldwide";

console.log(globalVar);


// alert() and prompt()

// alert() - allows us to show a small window at the top of our browser page to show information to our users. 

// alert("Hello User!");

// function showSampleAlert() {
// 	alert("Hello User!");
// }

// showSampleAlert();

// It, much like alert(), will have the page wait until the user completes or enters their input.


// =========================================


// prompt() - allows us to show a small window at the top of the browser to gather user input.

let samplePrompt = prompt("Enter your name: ");

console.log("Hello, " + samplePrompt);

function printWelcomeMessages() {
	let firstName = prompt("Enter your First Name");
	let lastName = prompt("Enter your Last Name");

	console.log("Hello, " + firstName + " " + lastName + "!");
	console.log("Welcome to Mobile Legends");
}

printWelcomeMessages(); 













































